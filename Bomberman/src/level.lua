levelC = 3

-- Z niezniszczalna ściana
-- X filary
-- Y pole wolne

-- A kawa
-- B box
-- T biurko
-- K kopiarka
-- M szafa

-- F kwiat
-- G kaktus

-- 1 konsuela
-- 2/5 korpoludek
-- 3 ochrona
-- 4 szef

level1 = {};

level1[0] =  {"Z","Z","Z","Z","Z","Z","Z","Z","Z","Z","Z","Z","Z"}
level1[1] =  {"Z","Y","Y","A","Y","Y","Y","K","Y","Y","Y","B","Z"}
level1[2] =  {"Z","Y","X","G","X","B","X","Y","X","Y","X","T","Z"}
level1[3] =  {"Z","T","1","Y","Y","Y","F","Y","Y","T","Y","Y","Z"}
level1[4] =  {"Z","Y","X","Y","X","Y","X","Y","X","A","X","Y","Z"}
level1[5] =  {"Z","M","Y","Y","A","Y","Y","Y","G","Y","Y","Y","Z"}
level1[6] =  {"Z","Y","X","B","X","Y","X","T","X","Y","X","Y","Z"}
level1[7] =  {"Z","Y","Y","Y","Y","B","K","Y","M","Y","Y","Y","Z"}
level1[8] =  {"Z","Y","X","T","X","Y","X","F","X","Y","X","B","Z"}
level1[9] =  {"Z","Y","K","Y","Y","G","Y","K","T","Y","Y","Y","Z"}
level1[10] = {"Z","Y","X","Y","X","Y","X","Y","X","A","X","1","Z"}
level1[11] = {"Z","1","Y","A","Y","M","Y","Y","F","Y","T","K","Z"}
level1[12] = {"Z","Z","Z","Z","Z","Z","Z","Z","Z","Z","Z","Z","Z"}


level1.x = 11
level1.y = 11

level2 = {};

level2[0] =  {"Z","Z","Z","Z","Z","Z","Z","Z","Z","Z","Z","Z","Z"}
level2[1] =  {"Z","Y","Y","A","Y","Y","Y","K","Y","Y","2","B","Z"}
level2[2] =  {"Z","Y","X","G","X","B","X","Y","X","Y","X","Y","Z"}
level2[3] =  {"Z","T","1","K","4","Y","F","Y","2","B","T","5","Z"}
level2[4] =  {"Z","Y","Y","Y","X","Y","X","Y","Y","A","X","1","Z"}
level2[5] =  {"Z","Z","Y","Y","A","Y","Y","Y","G","Y","Y","Y","Z"}
level2[6] =  {"Z","M","X","B","X","Y","X","T","X","Y","X","Z","Z"}
level2[7] =  {"Z","Y","Y","Y","2","B","K","Y","M","Y","2","B","Z"}
level2[8] =  {"Z","Y","Y","T","X","Y","Y","F","X","Y","X","Y","Z"}
level2[9] =  {"Z","Y","K","Y","Y","G","Y","Y","T","5","Y","Y","Z"}
level2[10] = {"Z","Y","X","Y","X","Y","T","Y","X","A","X","M","Z"}
level2[11] = {"Z","F","Y","A","Y","M","1","Y","F","Y","T","K","Z"}
level2[12] = {"Z","Z","Z","Z","Z","Z","Z","Z","Z","Z","Z","Z","Z"}

level2.x = 9
level2.y = 3

level3 = {};

level3[0] =  {"Z","Z","Z","Z","Z","Z","Z","Z","Z","Z","Z","Z","Z"}
level3[1] =  {"Z","Y","Y","K","1","Y","Y","Y","Y","Y","2","B","Z"}
level3[2] =  {"Z","Y","Z","T","5","X","Z","T","Y","Y","Y","F","Z"}
level3[3] =  {"Z","F","Z","X","4","K","Z","T","4","X","Z","Z","Z"}
level3[4] =  {"Z","M","2","B","Y","B","Z","T","Y","Y","2","B","Z"}
level3[5] =  {"Z","Z","Y","Y","Y","Y","Y","Y","Y","Y","3","G","Z"}
level3[6] =  {"Z","K","A","X","Y","Y","X","T","5","X","Z","Z","Z"}
level3[7] =  {"Z","Y","2","B","D","Y","Y","Y","Y","Y","Y","M","Z"}
level3[8] =  {"Z","Y","Y","Y","Y","A","Z","T","Y","Y","Y","M","Z"}
level3[9] =  {"Z","Y","F","X","4","A","Z","T","4","X","Y","Z","Z"}
level3[10] = {"Z","Y","2","B","Y","A","Z","T","Y","Z","Y","K","Z"}
level3[11] = {"Z","3","Y","Y","Y","Y","K","Y","Y","Z","1","M","Z"}
level3[12] = {"Z","Z","Z","Z","Z","Z","Z","Z","Z","Z","Z","Z","Z"}

level3.x = 5
level3.y = 9

level0={}
level0[0] =  {"Z","Z","Z","Z","Z","Z","Z","Z","Z","Z","Z","Z","Z"}
level0[1] =  {"Z","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Z"}
level0[2] =  {"Z","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Z"}
level0[3] =  {"Z","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Z"}
level0[4] =  {"Z","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Z"}
level0[5] =  {"Z","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Z"}
level0[6] =  {"Z","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Z"}
level0[7] =  {"Z","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Z"}
level0[8] =  {"Z","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Z"}
level0[9] =  {"Z","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Z"}
level0[10] = {"Z","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Z"}
level0[11] = {"Z","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Z"}
level0[12] = {"Z","Z","Z","Z","Z","Z","Z","Z","Z","Z","Z","Z","Z"}

level0.x = 5
level0.y = 5

function getLevel(n)
  if (n==nil) then
    return level1
  end
  
  if (n==1) then
    return level1
  end
    
  if (n==2) then
    return level2
  end
    
  if (n==3) then
    return level3
  end
  
  if (n==0) then
    return level0
  end

end 