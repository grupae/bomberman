require("src.level")
require("src.tools")
require("src.bomb")
require("src.enemy")
require("src.explosion")
require("src.sound")

function giveMeBoard(levelNr)
  local board = {}
  board.Margin=25
  board.x = board.Margin
  board.y = board.Margin
  local screenw = love.graphics:getWidth()
  local screenh = love.graphics:getHeight()
  board.w = screenh - 2 * board.Margin
  board.h = screenh - 2 * board.Margin
  board.sizeX = 13
  board.sizeY = 13
  board.status = "game"
  board.color = {}  
  board.color[0] = 255
  board.color[1] = 255
  board.color[2] = 255
  board.color[3] = 255
  board.bombs = nil
  board.enemys = {}
  board.enemysC = 0
  board.explosions = {}
  board.explosionPower = 2
  board.explosionTime = 0.5
  board.bombTime = 2.5
  board.tileSize = board.w/board.sizeX
  board.winx = 0
  board.winy = 0
  board.wino = {}
  board.winz = {}
  board.tiles = makeTilesFromLevel( getLevel(levelNr), board )
  return board
end

function bombExplode()
  playExplosion()
  board.explosions[#board.explosions+1] = Explosion:newExplosion(board, board.bombs.positionX, board.bombs.positionY)
  explodeInDirection("u", board.bombs.positionX, board.bombs.positionY)
  explodeInDirection("d", board.bombs.positionX, board.bombs.positionY)
  explodeInDirection("r", board.bombs.positionX, board.bombs.positionY)  
  explodeInDirection("l", board.bombs.positionX, board.bombs.positionY)  
  board.bombs=nil
end

function explodeInDirection(direction, startX, startY)
  local moveX = 0
  local moveY = 0
  
  if(direction=="u") then
    moveY=-1
  elseif(direction=="d") then
    moveY=1
  elseif(direction=="l") then
    moveX=-1
  elseif(direction=="r") then
    moveX=1
  end
  
  local powerLeft=board.explosionPower
  local tempX=startX
  local tempY=startY
  while(powerLeft>0) do
    tempX=tempX+moveX
    tempY=tempY+moveY
    if(board.tiles[tempX][tempY].kind=="Y") then
      board.explosions[#board.explosions+1] = Explosion:newExplosion(board, tempX, tempY)
    elseif(board.tiles[tempX][tempY].destroyable) then
      board.explosions[#board.explosions+1] = Explosion:newExplosion(board, tempX, tempY)
      
      tilesTable[tempX][tempY] = Tile:newOfKind("Y", tempX, tempY, board.x, board.y, board.tileSize)
      powerLeft=0
    else
      powerLeft=0
    end
        
    powerLeft=powerLeft-1
  end
  
end


function updateEnemys(board, dt)
  for i=board.enemysC, 1, -1 do
    board.enemys[i]:updateEnemy(board, dt)
  end
end

function updateExplosions(board, dt)
  local cond = true
  for i=1, #board.explosions, 1 do
    if (cond) then
      cond = board.explosions[i]:updateExplosion(board, dt)
    end
  end
end

function updateBombs(board, dt)
  if(board.bombs) then
      board.bombs:updateBomb(board, dt)
  end
end


function putBomb(board, rectangle)
  local a = 1
  if(board.bombs == nil) then
    board.bombs = Bomb:newBomb(rectangle)
    playSound("bombPlacement")
  end
end


function putBomb2(board, rectangle)
  if (#board.bombs < player.bomblimit) then
    
    local isClean = true
    for i=1, #board.bombs, 1 do
      if ( (board.bombs[i].x == rectangle.x) and (board.bombs[i].y == rectangle.y)) then
        isClean = false
      end
    end
    
    if(isClean) then
      board.bombs[#board.bombs+1] = Bomb:newBomb(rectangle)
    end
  end
end

function drawBoard(board,player)
  love.graphics.setColor(board.color[0],board.color[1],board.color[2],board.color[3])
  love.graphics.rectangle("fill", board.x,board.y, board.w, board.h)
  for i=0, #board.tiles, 1 do
    for j=0, #board.tiles[i], 1 do --jest jakas kaszanka tutaj, trzeba zbadac 
      board.tiles[i][j]:drawMe()
    end
  end
  
  if(board.tiles[board.winx][board.winy].kind == "Y") then
    if(board.enemysC==0) then
      board.wino:drawMe()
    else
      board.winz:drawMe()
    end
  end
  
  
  if(board.bombs) then
    board.bombs:drawMe()
  end
  --for i=1, #board.bombs, 1 do
  --  board.bombs[i]:drawMe()
  --end
  if(player.status~="live") then
    love.graphics.reset()
    drawPlayer(player)
  end
  
  for i=1, board.enemysC, 1 do
    board.enemys[i]:drawMe()
  end
  
  if(player.status=="live") then
    love.graphics.reset()
    drawPlayer(player)
  end
  
  for i=1, #board.explosions, 1 do
    board.explosions[i]:drawMe()
  end
  
end

function addEnemy(board, kind, nX, nY, nW, nH)
  board.enemysC=board.enemysC+1
  board.enemys[board.enemysC] = Enemy:newEnemy(kind, nX, nY, nW,nH)
end

function makeTilesFromLevel(levelTable, board)
  tilesTable = {}
  board.winx = levelTable.x
  board.winy = levelTable.y
  board.wino = Tile:newOfKind("WINO", levelTable.x, levelTable.y, board.x, board.y, board.tileSize)
  board.winz = Tile:newOfKind("WINZ", levelTable.x, levelTable.y, board.x, board.y, board.tileSize)
  for i=0, #levelTable, 1 do
    tilesTable[i] = {}
    
    for j=0, #levelTable, 1 do
      if (levelTable[j][i+1]=="1" or levelTable[j][i+1]=="2" or levelTable[j][i+1]=="3" or levelTable[j][i+1]=="4" or levelTable[j][i+1]=="5") then
        addEnemy(board, levelTable[j][i+1], board.x+i*board.tileSize, board.y+j*board.tileSize, board.tileSize,board.tileSize)
        tilesTable[i][j] = Tile:newOfKind("Y", i, j, board.x, board.y, board.tileSize)
      else
        local tempa = i
        local tempb = j
        tilesTable[i][j] = Tile:newOfKind(levelTable[j][i+1], i, j, board.x, board.y, board.tileSize)
      end
    end
  end
  return tilesTable
end


function checkColisionWithTiles(board, object)
  for i=0, #board.tiles, 1 do
    for j=0, #board.tiles[i], 1 do
      if (board.tiles[i][j]:isBlocking() and detectCollision(board.tiles[i][j], object) ) then
          return true
      end
    end
  end
  return false  
end

function checkColisionWithBombs(board, object)
  local result = false
  --for i=1, #board.bombs, 1 do
  --  if(detectCollision(board.bombs[i], object) ) then
  --    if(board.bombs[i]:isBlocking()) then
  --      result = true
  --   end
  --  else
  --    board.bombs[i]:setBlocking()
  --  end
  --end
  --return result
  if(board.bombs==nil)then
    return false
  end
  
  local hitbox = getHitbox(board.bombs)
  local rect = getRectangle(board.bombs)
  if(detectCollision(board.bombs, object) ) then
    if(board.bombs:isBlocking()) then
       return true
    end
  else
    if( not detectCollision(board.bombs, player) ) then
      board.bombs:setBlocking()
    end
    return false
  end
  
  return false
end



function checkColisionWithBoard(board, object)
  if (checkColisionWithBombs(board, object) ) then
    return true
  end
  
  if (checkColisionWithTiles (board, object) ) then
    return true
  end 
end

function checkColisionWithEnemys(board, object)
  local result = false
  for i=1, board.enemysC, 1 do
    if(detectCollision(board.enemys[i], object) ) then
      if(board.enemys[i].status=="live") then
        result = true
      end
    end
  end
  return result
end

function checkColisionWithExplosions(board, object)
  local result = false
  for i=1, #board.explosions, 1 do
    if(detectCollision(board.explosions[i], object) ) then
        result = true
    end
  end
  return result
end

function checkColisionWithWin(board, object)
  local result = false
  if(detectCollision(board.winz, object) ) then
      result = true
  end
  return result
end

function updateStatus(board,player)
  if(checkColisionWithEnemys(board, player) ) then
      --board.status = "dead"
      if(player.status ~= "dying" and player.deadTimer==0) then
        player.status = "dying"
        player.deadTimer = 0
      end
  end
  
  if(checkColisionWithExplosions(board, player) ) then
      --board.status = "dead"
      if(player.status ~= "dying") then
        player.status = "dying"
        player.deadTimer = 0
      end
  end

  
  if((board.tiles[board.winx][board.winy].kind == "Y") and (board.enemysC==0) and (checkColisionWithWin(board,player))) then
      board.status = "levelup"
  end
  
  if(player.status=="dead") then
    board.status = "dead"
  end
end